resource "google_sql_database_instance" "postgres_instance" {
  name             = "my-postgres-db"
  database_version = "POSTGRES_14"
  region           = "us-central1"
  settings {
    tier = "db-custom-1-3840"  // use the tier with atleast 100gb, 4-8cores and 4-8gb ram
    availability_type = "REGIONAL"
    disk_autoresize   = true
    disk_size         = 100
    disk_type         = "PD_SSD"
    maintenance_window {
      day = 0
      hour = 5
      update_track = "canary"
    }
  }
}

resource "google_sql_user" "postgres_user" {
  name     = "db_user"
  instance = google_sql_database_instance.postgres_instance.name
  password = "db_password"
}

resource "google_sql_database" "postgres_db" {
  name     = "my_database"
  instance = google_sql_database_instance.postgres_instance.name
}
