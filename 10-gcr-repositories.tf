resource "google_project_service" "artifact_registry" {
  project = "<your-gcp-project-id>"
  service = "artifactregistry.googleapis.com"
}

resource "google_artifact_registry_repository" "owl-agent" {
  project = "<your-gcp-project-id>"
  location      = "us-central1"
  repository_id = "owl-agent"
  description   = "example docker repository"
  format        = "DOCKER"

   docker_config {
    immutable_tags = true
  }
}

  resource "google_artifact_registry_repository" "owl-web" {
  project = "<your-gcp-project-id>"
  location      = "us-central1"
  repository_id = "owl-web"
  description   = "example docker repository"
  format        = "DOCKER"

   docker_config {
    immutable_tags = true
  }
}

  resource "google_artifact_registry_repository" "sparl" {
  project = "<your-gcp-project-id>"
  location      = "us-central1"
  repository_id = "spark"
  description   = "example docker repository"
  format        = "DOCKER"

   docker_config {
    immutable_tags = true
  }
}

  resource "google_artifact_registry_repository" "owl-livy" {
  project = "<your-gcp-project-id>"
  location      = "us-central1"
  repository_id = "owl-livy"
  description   = "example docker repository"
  format        = "DOCKER"

   docker_config {
    immutable_tags = true
  }
}
