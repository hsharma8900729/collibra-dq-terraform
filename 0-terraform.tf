# https://www.terraform.io/language/settings/backends/gcs
terraform {
  required_version = ">= 0.14"
  backend "gcs" {
    bucket = "<your-bucket-name>"
    prefix = "terraform/state"
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.0"
    }
  }
}