# https://registry.terraform.io/providers/hashicorp/google/latest/docs
provider "google" {
  project = "<your-gcp-project-id>"
  region  = "us-central1"
  credentials = "${file("path/to/credential.json")}"
}

